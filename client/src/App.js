import React, { Suspense, useEffect,useLayoutEffect } from 'react';
import './App.css';
import { Switch, Route } from "react-router-dom"
import Header from "./components/Header"
import Footer from "./components/Footer"
import {useLocation,withRouter} from "react-router-dom"
const Anasayfa = React.lazy(() => import('./page/Anasayfa'));
const FaaliyetAlanlarimiz = React.lazy(() => import('./page/Faaliyetalanlarimiz'));
const Bloglar = React.lazy(() => import('./page/Bloglar'));
const Hakkimizda = React.lazy(() => import('./page/Hakkimizda'));
const Iletisim = React.lazy(() => import('./page/Iletisim'));
const ScrollToTop = withRouter(({ children, location: { pathname } }) => {
  useLayoutEffect(() => {
    window.scrollTo(0, 0)
  }, [pathname])

  return children || null
})
function App() {
  const path = useLocation().pathname
  useEffect(() => {
    setTimeout(function(){     
      window.location.reload(1);
     }, 1000*60*20);
  })
  useEffect(() => {
    function loadScript(src) {    
      return new Promise(function(resolve, reject){
        var script = document.createElement('script');
        script.src = src;
        script.addEventListener('load', function () {
          resolve();
        });
        script.addEventListener('error', function (e) {
          reject(e);
        });
        document.body.appendChild(script);
        document.body.removeChild(script);
      })
    
    };

  loadScript("/js/theme.js")
  loadScript("/js/theme.init.js") 
  }, [path])
  return (
    <>
      <Header />
      {/* <ScrollToTop />     */}
      <ScrollToTop> 
      <div role="main" className="main">
     
        <Switch>
          <Suspense fallback={<div>Yükleniyor...</div>}>
            <Route exact path='/' component={Anasayfa}></Route>
       
            <Route exact path='/faaliyet-alanlarimiz' component={FaaliyetAlanlarimiz}></Route>
        
            <Route exact path='/makaleler' component={Bloglar}></Route>
      
            <Route exact path='/hakkimizda' component={Hakkimizda}></Route>
         
            <Route exact path='/iletisim' component={Iletisim}></Route>
          </Suspense>
        </Switch>
      
        <Footer />
      </div>
      </ScrollToTop>
    </>
  );
}

export default App;
