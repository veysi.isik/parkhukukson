const express = require("express")
const app = express()
const dotenv = require('dotenv');
const nodemailer = require('nodemailer')
const getAllPosts = require("./routers/getAllPosts")
const cors = require('cors')
const buildPath = path.join(__dirname, '..', 'build');
app.use(express.static(buildPath));

app.use(cors());
dotenv.config();
// Middeleware
app.use(express.json())




app.use("/iletesim", getAllPosts)
// 404 Catcher

app.use((req, res, nex) => {
    const error = new Error("URL Not Found");
    error.status = 404;
    nex(error);
})
app.use((req, res, nex) => {
    res.status(error.status || 500);
    res.send({ message: `Error! ${error.message}`, error: error })

})
const port = process.env.PORT || 5000;
app.listen(port, () => console.log(`server running on port ${port}`))